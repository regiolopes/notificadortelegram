const zabbixAPI = require("../../config/axios_zabbix");
const telegram = require("../../config/axios");
const dotenv = require("dotenv").config()
const database_zabbix = require("../../data/database_zabbix.json")

var zabbix = {};


const logout = function (token) {
    return new Promise((resolveauth, rejectauth) => {
      var bodyauth = {
        jsonrpc: "2.0",
        method: "user.logout",
        params: [],
        id: 1,
        auth: token,
      };
      zabbixAPI
        .post("api_jsonrpc.php", bodyauth)
        .then((req) => {
          let result = req.data.result;
          // console.log(req.data)
          resolveauth(result);
        })
        .catch((err) => {
          console.log(err);
          rejectauth("Erro ao autenticar!");
        });
    });
  };

zabbix.ZimbraemailsDeferred = function emailsDeferred() {
  const auth = new Promise((resolveauth, rejectauth) => {
    var bodyauth = {
      jsonrpc: "2.0",
      method: "user.login",
      params: {
        user: process.env.ZABBIX_USER,
        password: process.env.ZABBIX_PASSWORD,
      },
      id: process.env.ZABBIX_ID,
      auth: null,
    };
    zabbixAPI
      .post("api_jsonrpc.php", bodyauth)
      .then((req) => {
        let result = req.data.result;
        console.log("Usuario Zabbix autenticado!");
        resolveauth(result);
      })
      .catch((err) => {
        console.log(err);
        rejectauth("Erro ao autenticar!");
      });
  });


  const sendMessage = new Promise((resolve, reject) => {
    let tokenauth;
    auth
      .then((res) => {
        var itemID = database_zabbix.hosts[4].FilaDeferred

        tokenauth = res;
        var body = {
          jsonrpc: "2.0",
          method: "item.get",
          params: {
            filter: {
              itemid: itemID,
            },
          },
          auth: tokenauth,
          id: 1,
        };

        zabbixAPI
          .post("api_jsonrpc.php", body)
          .then((req) => {
            let datart = {
              Nome: req.data.result[0].name,
              CurrentValue: req.data.result[0].lastvalue,
              PrevValue: req.data.result[0].prevvalue,
            };
            // datart.CurrentValue = 50
            let limitdeferred = parseInt(process.env.ALERT_ZIMBRA_FILADEFERRED)

            if (datart.CurrentValue >= limitdeferred) {
      
              var msg = `⚠ ⚠ ⚠  ${datart.Nome} está com valor ${datart.CurrentValue}! || Valor anterior: ${datart.PrevValue}`;

              var params = new URLSearchParams();
              params.append("chat_id", "-1001671173405");
              params.append("text", msg);
              params.append("parse_mode", "Markdown");

              telegram.post("sendMessage", params).then((e) => {
                console.log(`Mensagem Enviada: ${msg}`);
              });
              resolve(datart);

            } else {

            }
            
          })
          .catch((err) => {
            console.log(err);
          });

          setTimeout(() => {
            logout(tokenauth)
            .then((res) => {
              if (res) {
                console.log("USUÁRIO Zabbix DESLOGADO");
              } else {
                // console.log(res)
                console.log(
                  "Erro para deslogar | Usuário será deslogado dps de 15 min"
                );
              }
            })
            .catch((err) => {
              console.log(
                "Erro para deslogar | Usuário será deslogado dps de 15 min"
              );
            });
          },5000)
      })
      .catch((err) => {
        reject("Error ao buscar a autenticação");
      });
  });
};

zabbix.ICMPping = function () {
    const auth = new Promise((resolveauth, rejectauth) => {
      var bodyauth = {
        jsonrpc: "2.0",
        method: "user.login",
        params: {
          user: process.env.ZABBIX_USER,
          password: process.env.ZABBIX_PASSWORD,
        },
        id: process.env.ZABBIX_ID,
        auth: null,
      };
      zabbixAPI
        .post("api_jsonrpc.php", bodyauth)
        .then((req) => {
          let result = req.data.result;
          console.log("Usuario Zabbix autenticado!");
          resolveauth(result);
        })
        .catch((err) => {
          console.log(err);
          rejectauth("Erro ao autenticar!");
        });
    });
  
  
    const sendMessage = new Promise((resolve, reject) => {

      let tokenauth;
      auth
        .then((res) => {
          tokenauth = res;
          var hosts = database_zabbix.hosts
          hosts.map(item => {
            var body = {
              jsonrpc: "2.0",
              method: "item.get",
              params: {
                filter: {
                  itemid: item.ICMPping,
                },
              },
              auth: tokenauth,
              id: 1,
            };

    
            zabbixAPI
              .post("api_jsonrpc.php", body)
              .then((req) => {
                let datart = {
                  Nome: req.data.result[0].name,
                  NomeHost: item.nome,
                  IDHost : item.id,
                  CurrentValue: req.data.result[0].lastvalue,
                  // CurrentValue: 0,
                  PrevValue: req.data.result[0].prevvalue,
                };

                
              //  console.log(datart)
                if (datart.CurrentValue == 0) {
                  var msg = `⚠ ⚠ ⚠ ${datart.NomeHost} down! \n `;
    
                  var params = new URLSearchParams();
                  params.append("chat_id", "-1001671173405");
                  params.append("text", msg);
                  params.append("parse_mode", "Markdown");
    
                  telegram.post("sendMessage", params).then((e) => {
                    console.log(`Mensagem Enviada: ${msg}`);
                  })
                  .catch(err => {
                    console.log(err.response.data)
                  });
                  
                  resolve(datart);
                  
                } else {
                  
                }
              })
              .catch((err) => {
                console.log(err);
              });
          })
          setTimeout(() => {
            logout(tokenauth)
            .then((res) => {
              if (res) {
                console.log("USUÁRIO Zabbix DESLOGADO");
              } else {
                // console.log(res)
                console.log(
                  "Erro para deslogar | Usuário será deslogado dps de 15 min"
                );
              }
            })
            .catch((err) => {
              console.log(
                "Erro para deslogar | Usuário será deslogado dps de 15 min"
              );
            });
          },5000)
          
   
        })
        .catch((err) => {
          reject("Error ao buscar a autenticação");
        });
    });
  };

module.exports = zabbix;
