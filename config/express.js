const express = require("express");
const bodyParser = require("body-parser");
var cron = require("node-cron");
const sembackup = require("../src/services/bancosembackup");
const alertaCPU = require("../src/services/alertaCPU");
const jobsFalharam = require("../src/services/jobsFalharam");
const zabbix = require("../src/services/zabbix");


module.exports = () => {
	const app = express();

	// ALERTAS SQL SERVER 

	/// ALERTAS ZABBIX
		cron.schedule("*/5 * * * *", () => {
			zabbix.ICMPping()
		});

		cron.schedule("*/1 * * * *", () => {
			zabbix.ZimbraemailsDeferred();
		});
		
	// MIDDLEWARES
	app.use(bodyParser.json());
	app.use(
		bodyParser.urlencoded({
			extended: true,
		})
	);

	return app;
};
