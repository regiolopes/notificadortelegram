const axios = require("axios");

const zabbix = axios.create({
	baseURL: "http://192.168.1.2/zabbix/",
	timeout: 30000,
	headers: {"Content-Type" : "application/json-rpc"}
})

module.exports = zabbix;

