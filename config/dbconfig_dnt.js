const dotenv = require("dotenv");

const env = dotenv.config().parsed;

const configDNT = {
	user: env.DB_USER_DNT,
	password: env.DB_PWD_DNT,
	server: env.DB_SERVER_DNT,
	database: env.DB_NAME_DNT,
	options: {
		trustedconnection: true,
		enableArithAort: true,
		instancename: "MSSQLSERVER",
		cryptoCredentialsDetails: {
			minVersion: "TLSv1",
		},
		encrypt: false,
		trustServerCertificate: false,
	},
	port: 1433,
	connectionTimeout: 5000,
	requestTimeout: 5000,
};

module.exports = configDNT;
