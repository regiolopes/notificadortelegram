const configDNT = require("./dbconfig_dnt.js");
const sql = require("mssql");

module.exports = function execsql_dnt_client(func) {
	const pool1 = new sql.ConnectionPool(configDNT);

	// //WATCH MUDANCAS DE CLIENTE
	// pool1.on("error", (err) => {
	// 	console.log(
	// 		"\n\n\n------------------ERRO NA POOL kakakakakakakakakakakakakakak----------------------\n\n\n\n",
	// 		err
	// 	);
	// });
	pool1.on("connection", (connection) => {
		console.log(
			"\n\n\n------------------CONNECTION ADQUIRIDA----------------------\n\n\n\n",
			connection
		);
	});
	// pool1.on("release", (err) => {
	// 	console.log(
	// 		"\n\n\n------------------ERRO NA POOL----------------------\n\n\n\n",
	// 		err
	// 	);
	// });
	pool1.on("enqueue", (err) => {
		console.log(
			"\n\n\n------------------ENQUEUE NA POOL----------------------\n\n\n\n",
			err
		);
	});
		pool1.on("error", (err) => {
			console.log(
				"\n\n\n------------------ERRO NA POOL----------------------\n\n\n\n",
				err
			);
		});

	const after = new Promise((resolve, reject) => {
		pool1
			.connect()
			.then((client) => {
				func({
					client: client,
					release: () => {
						// console.log("------------CLIENTE LIBERADO-------");
						resolve("Cliente liberado");
					},
					sql:sql,
					pool:pool1
				});
			})
			.catch((e) => {
				console.log(e)
				console.log("------------ERRO DE CONEXÃO COM O BANCO-------");
				reject("Falha de Conxão com o banco, incapaz de conectar pool  ");
			});
	});
	return after;
};
