const axios = require("axios");
const dotenv = require("dotenv").config()

bot_id = process.env.TELEGRAM_BOT_ID

const telegram = axios.create({
	// baseURL: " https://api.telegram.org/bot5298727222:AAFF8IioiBJfoZky2T_B_bT7kBbDWcq7Wwc/",
	baseURL: `https://api.telegram.org/${bot_id}/`,
	timeout: 300000,
	headers: { "Content-Type": "application/x-www-form-urlencoded" },
});

module.exports = telegram;



// https://api.telegram.org/bot5298727222:AAFF8IioiBJfoZky2T_B_bT7kBbDWcq7Wwc/sendMessage