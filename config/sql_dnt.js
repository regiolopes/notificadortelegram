const configDNT = require("./dbconfig_dnt.js");
const sql = require("mssql");

const pool1 = new sql.ConnectionPool(configDNT);
const pool1Connect = pool1.connect();

pool1.on("error", (err) => {
	console.log('\n\n\n------------------ERRO NA POOL----------------------\n\n\n\n', err);
});
pool1Connect
	.then((e) => {
		console.log("\n\n\n\n-----------------SUCESSO AO CONECTAR POOL-----------------\n\n\n");
	})
	.catch((err) => {
		console.log("\n\n\n\n----------------ERRO AO CRIAR POOL------------------------\n\n\n\n");
	});

module.exports = async function execsql_dnt(sqlQuery) {

	await pool1Connect; // ensures that the pool has been created
	try {
		const request = pool1.request(); // or: new sql.Request(pool1)
		const result = await request.query(sqlQuery);


		return result;
	} catch (err) {
		// console.error('SQL error', err);
		return err;
	}
};
