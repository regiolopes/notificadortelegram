        
        var express = require('express');
        var consign = require('consign');
        const cors = require('cors');
        var app = require('./config/express')();

        var corsOptions = {
            origin: '*',
            optionsSuccessStatus: 200, // For legacy browser support
            methods: "*"
        }
        
        app.use(cors(corsOptions));
        
        app.use(express.static('public'))

        app.use(express.json());

        app.use(express.urlencoded({ extended: true }));

        // const fileUpload = require('express-fileupload');
        // app.use(fileUpload());

        app.set('view engine', 'ejs');

        consign()
            .include('src/routes')
            .then('src/models')
            .then('src/controllers')
            .into(app);

        app.use(function (req, res, next) {

            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Headers', 'Content-Type, X-Custom-Header');
            // res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            // res.setHeader('Access-Control-Allow-Credentials', false);

            // // Request methods you wish to allow
            // res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            
            // // Request headers you wish to allow
            // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            
            // // Set to true if you need the website to include cookies in the requests sent
            // // to the API (e.g. in case you use sessions)
            // res.setHeader('Access-Control-Allow-Credentials', true);
            
            // Pass to next layer of middleware

            next();
        });


        var http = require('http').createServer(app);

        // chat.chat(http)
        // notification.notification(http)

        http.listen(8006, function(){
            console.log('APP rodando na porta 8006');
        });


